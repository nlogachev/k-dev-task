<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTransactionRequest;
use App\Http\Requests\ListTransactionsRequest;
use App\Services\LockerService as Locker;
use App\Transaction;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resources.
     *
     * @param  \App\Http\Requests\ListTransactionsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function index(ListTransactionsRequest $request)
    {
        // Date constraints
        try {
            $fromDate = ($request->has('from_date')) ? Carbon::createFromFormat(\DateTime::ISO8601, $request->get('from_date')) : Carbon::now()->subWeek();
            $toDate = ($request->has('to_date')) ? Carbon::createFromFormat(\DateTime::ISO8601, $request->get('to_date')) : Carbon::now();
        } catch (\Exception $e) {
            return $this->errorResponse(400, 'Invalid dates');
        }

        // Make sure dates are properly ordered
        if ($fromDate->gte($toDate)) {
            return $this->errorResponse(400, 'Invalid dates');
        }

        // For later
        $data = [];

        // Ordering
        $order = ($request->get('order')) ?: 'desc';

        // Do we need to display a report or a listing?
        $aggregate = ($request->get('aggregate')) ?: 'none';

        if ($aggregate == 'none') {

            $data = $this->simpleList($request, $fromDate, $toDate, $order);

        } else {

            // The report should really be in a separate route and controller,
            // but we'll simplify things here.
            $data = $this->report($request, $fromDate, $toDate, $order, $aggregate);

        }

        return $this->dataResponse($data);
    }

    /**
     * Get a list of transaction models based on the request criteria
     *
     * @param  \App\Http\Requests\ListTransactionsRequest  $request
     * @param  \Carbon\Carbon  $fromDate
     * @param  \Carbon\Carbon  $toDate
     * @param  string  $order
     * @return \Illuminate\Http\Response
     */
    protected function simpleList(ListTransactionsRequest $request, Carbon $fromDate, Carbon $toDate, $order)
    {
        // Handle pagination
        $page = ($request->get('page')) ?: 1;
        $limit = ($request->get('limit')) ?: 100;

        // In this case we're just getting individual models, so it's a 
        // straightforward query we're after.
        $data = Transaction::where('created_at','>=',$fromDate)
            ->where('created_at','<=',$toDate)
            ->skip(($page - 1) * $limit)
            ->take($limit)
            ->orderBy('created_at', $order)
            ->get();

        return $data;
    }

    /**
     * Put together a transaction report based on the request criteria
     *
     * @param  \App\Http\Requests\ListTransactionsRequest  $request
     * @param  \Carbon\Carbon  $fromDate
     * @param  \Carbon\Carbon  $toDate
     * @param  string  $order
     * @param  string  $aggregate
     * @return \Illuminate\Http\Response
     */
    protected function report(ListTransactionsRequest $request, Carbon $fromDate, Carbon $toDate, $order, $aggregate)
    {
        // Ideally, what we'd actually do is have a separate Aggregation
        // model, which acts as another layer from the live data. It would
        // drastically cut down on realtime transaction processing and 
        // database impact at the cost of some upfront code complexity.
        // So if you wanted a report of daily deposits from last week,
        // you'd just search the database for the 7 individual day
        // aggregation models rather than the thousands of individual 
        // transactions.
        // This is a bit beyond the scope of this task, so we'll just leave
        // this note about it here. :)

        // Set up the base queries
        $query = DB::table('transactions')
            ->select([
                DB::raw('SUM(amount) as total'),
                DB::raw('DATE(created_at) as date'),
                DB::raw('COUNT(id) as count'),
                'country', 
                'transaction_type',
            ])
            ->where('created_at','>=',$fromDate)
            ->where('created_at','<=',$toDate)
            ->orderBy(DB::raw('DATE(created_at)'), 'desc')
            ->orderBy('country','asc')
            ->groupBy(DB::raw('DATE(created_at)'))
            ->groupBy('country')
            ->groupBy('transaction_type');

        $customerQuery = DB::table('transactions')
            ->select([
                DB::raw('DATE(created_at) as date'),
                DB::raw('COUNT(DISTINCT user_id) as users'),
                'country',
            ])
            ->where('created_at','>=',$fromDate)
            ->where('created_at','<=',$toDate)
            ->groupBy(DB::raw('DATE(created_at)'))
            ->groupBy('country');
        
        // This whole thing would - in more complex scenario - be abstracted 
        // out into a Repository pattern, looking something like:
        //
        // $transactions->from($fromDate)->to($toDate)->daily();
        //
        $results = $query->get();

        $data = [];

        // Now we just need to combine the separate transaction types into 
        // single rows
        $results->each(function($result) use (&$data) {
            
            $key = $result->date . $result->country;

            if (!isset($data[$key])) {
                $data[$key] = [
                    'date' => $result->date,
                    'country' => $result->country,
                    'deposited' => 0,
                    'deposits' => 0,
                    'withdrawn' => 0,
                    'withdrawals' => 0,
                ];
            }

            if ($result->transaction_type == Transaction::TYPE_DEPOSIT) {
                $data[$key]['deposited'] = $result->total;
                $data[$key]['deposits'] = $result->count;
            } else if ($result->transaction_type == Transaction::TYPE_WITHDRAW) {
                $data[$key]['withdrawn'] = - $result->total;
                $data[$key]['withdrawals'] = $result->count;
            }

        });

        // Finally we'll add the unique customer results
        $results = $customerQuery->get();
        $results->each(function($result) use (&$data) {

            $key = $result->date . $result->country;

            $data[$key]['users'] = $result->users;

        });

        return array_values($data);
    }

    /**
     * Display a single instance of the resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);

        if (!$transaction) {

            return $this->errorResponse(404, 'Transaction not found');

        }

        return $this->dataResponse($transaction);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTransactionRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTransactionRequest $request)
    {
        // Does the user actually exist?
        $user = User::find($request->get('user_id'));
        if (!$user) {
            return $this->errorResponse(404, 'User not found');
        }

        // For reference...
        $amount = $request->get('amount');
        $type = $request->get('transaction_type');
        $originalBalance = $user->balance;
        $originalBalanceBonus = $user->balance_bonus;

        // Make sure the transaction amounts are at least the minimum required.
        // Normally we'd move this to a business logic module, but here that 
        // would be overkill...
        if ($type == Transaction::TYPE_WITHDRAW && $amount < Transaction::MIN_WITHDRAWAL) {
            return $this->errorResponse(400, 'Withdrawals must be at least ' . Transaction::MIN_WITHDRAWAL);
        } else if ($type == Transaction::TYPE_DEPOSIT && $amount < Transaction::MIN_DEPOSIT) {
            return $this->errorResponse(400, 'Deposits must be at least ' . Transaction::MIN_DEPOSIT);
        }

        // Can't withdraw more than the balance
        if ($type == Transaction::TYPE_WITHDRAW && $originalBalance - $amount < 0) {
            return $this->errorResponse(400, 'Can\'t withdraw more than the balance');
        }

        // In an environment with a single server instance or with the 
        // possibility of using sticky sessions in the load balancer, we can
        // use simple file locks for data consistency in concurrent requests.
        // We create a lock file per user, since transactions here only 
        // concern conflicts scenarios on a per-user basis.

        // Try to acquire lock     
        $fp = null;   
        $locked = Locker::lockTransaction($user->id, $fp);

        // Just to show the idea... but read on for an alternative approach
        if (!$locked) {
            return $this->errorResponse(409, 'Concurrent transactions per user not allowed');
        }

        // Prepare our final response object.
        $json = [];

        // We're going to wrap this whole thing in a try/catch/finally block
        // so that we can be sure to free the lock we just created regardless
        // of what happens.
        try {

            // In an environment where a single server instance that processes DB
            // requests is not guaranteed (opaque load balancing), we can fall
            // back on a more straight-forward solution: making updates 
            // completely unambiguous (and combining this with InnoDB row-level 
            // locking, for example).

            // Prepare our base query. What we're doing here is making sure that 
            // the update query will look for the STATE that the user was in when 
            // this request started.
            $userUpdateQuery = User::where('balance', $originalBalance)
                ->where('balance_bonus', $originalBalanceBonus)
                ->where('id', $user->id);

            // Set up our actual monetary transaction model.
            $transaction = new Transaction();
            $transaction->transaction_type = $type;
            $transaction->amount = $amount;
            $transaction->user_id = $user->id;
            $transaction->country = $user->country;

            // Every third deposit we want to reward the user with their random
            // bonus.
            $newBalanceBonus = $originalBalanceBonus;
            if ($type == Transaction::TYPE_DEPOSIT) {
                
                // How may deposits have there been already?
                $deposits = Transaction::where('user_id',$user->id)->where('transaction_type', Transaction::TYPE_DEPOSIT)->count();

                // Will this one be a multiple of three?
                if (($deposits + 1) % 3 == 0) {
                    $newBalanceBonus += $user->bonus * $amount;
                }

            } else if ($type == Transaction::TYPE_WITHDRAW) {
                // Subtract the withdrawal amount from the user's balance
                $amount *= -1;
            }
            $newBalance = $originalBalance + $amount;

            // Wrap the DB updates in a transaction that will auto-rollback on failure
            DB::transaction(function() use ($userUpdateQuery, $originalBalance, $newBalanceBonus, $newBalance, $transaction) {
                // We want both saves to be successful, otherwise roll back
                // both by throwing an exception inside the transaction.
                $saved = $userUpdateQuery->update(['balance' => $newBalance, 'balance_bonus' => $newBalanceBonus]) && $transaction->save();

                if (!$saved) {
                    // This should obviously be more specific...
                    throw new Exception("Error Processing Request", 1);
                }
            });

            $json = ['success' => true];

        } catch (\Exception $e) {
            
            // Do some logging and further processing in real life.
            Log::error($e);

            $json = ['error' => 500, 'message' => 'Internal server error.'];

        } finally {
            
            // Clean up the lock for the next request from the same user.
            Locker::unlockTransaction($fp);

            // To account for possible failures (for whatever reason), we can
            // set up a recurring job that checks for existing lock files and
            // removes them if they're older than a couple of seconds.

        }

        // And we're done...
        return (!isset($json['error'])) ? $this->successResponse() : $this->errorResponse($json['error'], $json['message']);

    }
}
