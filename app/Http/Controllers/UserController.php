<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller
{

    /**
     * Display a single instance of the resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);

        if (!$user) {

            return $this->errorResponse(404, 'User not found');

        }

        return $this->dataResponse($user);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request)
    {
        $email = $request->get('email');

        // Check if there's an existing user with that email
        $user = User::where('email', $email)->first();

        if ($user) {
            return $this->errorResponse(409, 'Email in use');
        }

        // Create the new user model
        $user = new User();
        $user->email = $email;
        $user->first_name = $request->get('first_name');
        $user->last_name = $request->get('last_name');
        $user->country = $request->get('country');
        $user->gender = $request->get('gender');

        // Add the random bonus
        $user->bonus = User::randomBonusAmount();

        // In this case it's super simple, but we'll likely be doing a lot more
        // work than this, in which case database transactions are a must.
        try {
            DB::transaction(function() use ($user) {
                if (!$user->save()) {
                    // Just a filler exception to show the idea
                    throw new Exception("Error Processing Request", 1);
                }
            });
        } catch (\Exception $e) {
            // Logging or further processing
            Log::error($e);

            return $this->errorResponse(500, 'Internal server error');
        }
        
        return $this->successResponse();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUserRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUserRequest $request, $id)
    {
        // Try to first find the model in the db
        $user = User::find($id);
        if (!$user) {
            return $this->errorResponse(404, 'User not found');
        }

        // Explicitly reject request if it's trying to update balances. Other 
        // unexpected data is simply ignored.
        if ($request->has('balance') || $request->has('balance_bonus')) {
            return $this->errorResponse(400, 'Balance cannot be updated directly');
        }

        // Update relevant data
        if ($request->has('email')) {
            $user->email = $request->get('email');
        }
        if ($request->has('first_name')) {
            $user->first_name = $request->get('first_name');
        }
        if ($request->has('last_name')) {
            $user->last_name = $request->get('last_name');
        }
        if ($request->has('gender')) {
            $user->gender = $request->get('gender');
        }
        if ($request->has('country')) {
            $user->country = $request->get('country');
        }

        // In this case it's super simple, but we'll likely be doing a lot more
        // work than this, in which case database transactions are a must.
        try {
            DB::transaction(function() use ($user) {
                if (!$user->save()) {
                    // Just a filler exception to show the idea
                    throw new Exception("Error Processing Request", 1);
                }
            });
        } catch (\Exception $e) {
            // Logging or further processing
            Log::error($e);

            return $this->errorResponse(500, 'Internal server error');
        }
        
        return $this->successResponse();
    }

}
