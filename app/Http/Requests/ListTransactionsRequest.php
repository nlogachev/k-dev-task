<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ListTransactionsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aggregate' => 'in:daily,none',
            'order' => 'in:asc,desc',
            'page' => 'integer|min:1',
            'limit' => 'integer|min:1|max:1000',
        ];
    }
}
