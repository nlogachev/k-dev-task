<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Transaction;

class StoreTransactionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $types = Transaction::types();

        return [
            'amount' => 'required|numeric|min:0.01',
            'transaction_type' => 'required|in:'.implode(',', $types),
            'user_id' => 'required|integer|min:1',
        ];
    }
}
