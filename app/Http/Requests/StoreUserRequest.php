<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\User;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => $this->emailRules(),
            'gender' => $this->genderRules(),
            'first_name' => $this->firstNameRules(),
            'last_name' => $this->lastNameRules(),
            'country' => $this->countryRules()
        ];
    }

    protected function emailRules($required = true)
    {
        return $this->buildRules('email|max:255', $required);
    }

    protected function genderRules($required = true)
    {
        $genders = User::genders();

        return $this->buildRules('in:'.implode(',', $genders), $required);
    }

    protected function firstNameRules($required = true)
    {
        return $this->buildRules('min:2|max:48', $required);
    }

    protected function lastNameRules($required = true)
    {
        return $this->firstNameRules($required);
    }

    protected function countryRules($required = true)
    {
        // We'd normally have some kind of helper function, but we'll keep 
        // things simple here just for demonstration purposes.
        $countries = ['DE','MT'];

        return $this->buildRules('in:'.implode(',', $countries), $required);
    }

    protected function buildRules($rules, $required = true)
    {
        $prefix = ($required) ? 'required|' : '';

        return $prefix . $rules;
    }
}
