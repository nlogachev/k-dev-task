<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends StoreUserRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => $this->emailRules(false),
            'gender' => $this->genderRules(false),
            'first_name' => $this->firstNameRules(false),
            'last_name' => $this->lastNameRules(false),
            'country' => $this->countryRules(false)
        ];
    }
}
