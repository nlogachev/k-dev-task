<?php

namespace App\Services;

class LockerService
{
	protected static $transactionFolder = 'app/locks/transactions/';

	public static function lockTransaction($userId, &$fp)
	{
        // Make sure the lock folder is available
        if (!is_dir(storage_path(self::$transactionFolder))) {
            @mkdir(storage_path(self::$transactionFolder), 0755, true);
        }

        // Create and open the lock file
        touch(storage_path(self::$transactionFolder . $userId . '.lock'));
        $fp = fopen(storage_path(self::$transactionFolder . $userId . '.lock'), 'r+');

        return flock($fp, LOCK_EX);
	}

	public static function unlockTransaction($fp)
	{
		@flock($fp, LOCK_UN);
        @fclose($fp);
	}
}