<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
	const TYPE_DEPOSIT = 1;
	const TYPE_WITHDRAW = 2;
	const TYPE_REFUND = 3; // Not in use here, but in real life...

	// With multiple currencies obviously this wouldn't be so simple. We'd have
	// this in another module.
	const MIN_DEPOSIT = 5;
	const MIN_WITHDRAWAL = 100;

	public static function types()
	{
		return [
			self::TYPE_DEPOSIT,
			self::TYPE_WITHDRAW
		];
	}   
}
