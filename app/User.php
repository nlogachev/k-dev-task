<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    const BONUS_MIN = 0.05;
    const BONUS_MAX = 0.20;

    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_TRANS_MALE = 3; // Just as a note that these days there's more
    // to genders than the classic male/female split, so modern apps need to
    // take this into account.

    /**
     * The attributes that should be auto-hidden from JSON display.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', // This is just to show that we're keeping 
        // this in mind
    ];

    public static function genders()
    {
        return [
            self::GENDER_MALE,
            self::GENDER_FEMALE,
            self::GENDER_TRANS_MALE,
        ];
    }

    public static function randomBonusAmount()
    {
        return mt_rand(self::BONUS_MIN * 100, self::BONUS_MAX * 100) / 100;
    }
}
