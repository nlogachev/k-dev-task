<?php

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'gender' => 1,
        'country' => 'DE',
        'bonus' => App\User::randomBonusAmount(),
        'balance' => 10,
        'balance_bonus' => 1.6
    ];
});

$factory->define(App\Transaction::class, function (Faker\Generator $faker) {
    return [
        'country' => 'MT',
        'amount' => 10,
        'user_id' => 1,
        'transaction_type' => App\Transaction::TYPE_DEPOSIT
    ];
});