<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('first_name', 48); // Long names, but not 255-character long.
            $table->string('last_name', 48);
            $table->tinyInteger('gender')->unsigned();
            $table->string('country', 2); // 2-letter ISO code

            $table->decimal('bonus', 5, 4);
            $table->decimal('balance', 5, 4)->default(0);
            $table->decimal('balance_bonus', 5, 4)->default(0);

            $table->string('email', 255)->unique();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
