<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');

            $table->tinyInteger('transaction_type');
            $table->decimal('amount', 12, 4); // Keep sub-cent amounts available for accuracy.
            $table->integer('user_id')->unsigned();
            $table->string('country', 2); // 2-letter ISO code

            $table->timestamps();

            $table->index('created_at');
            $table->index('user_id');
            $table->index('country'); // Only really relevant when operating globally
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
