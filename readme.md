# "K" Dev Task

[![CircleCI](https://circleci.com/bb/nlogachev/k-dev-task.svg?style=svg)](https://circleci.com/bb/nlogachev/k-dev-task)

## Overview

### Input Validation

This happens in custom Requests which are dependency-injected into the controllers:

`app/Http/Requests/`

### Database structure

Available at a glance in the migration files:

`database/migrations/`

### Routing

RESTful endpoints are declared in:

`routes/api.php`

### Throttling

Handled automatically on API routes by Laravel

### Main endpoint processing

`app/Http/Controllers/`

### Models

`app/User.php` and `app/Transaction.php`

### Tests

`tests/`


## Prerequisistes

### PHP >= 5.6.4 with mcrypt and openssl

If unavailable, can be installed (on Mac) with [Homebrew](http://brew.sh/):

`brew install php56 php56-mcrypt openssl`

For command-line usage, curl is recommended as an extra dependency.

### Composer

`cd /path/where/you/want/to/install/composer/perhaps/one/level/above/project/root`

```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === 'aa96f26c2b67226a324c27919f1eb05f21c248b987e6195cad9690d5c1ff713d53020a02ac8c217dbf90a7eacc9d141d') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```

### Database

An SQLite database is used, so no additional database installation or setup is necessary.



## Installation

### Pull in git repository

`cd /path/to/folder/one/level/above/intended/project/root`

```
git clone https://bitbucket.org/nlogachev/k-dev-task.git
cd k-dev-task
```

### Setup dependencies

`/path/where/you/installed/composer.phar install`


### (Optional) Set up the database files

The post-install script in `composer.json` should take care of creating the appropriate database files for you. In case they're not created for some reason, you can run the following command from the project's root directory:

`touch database/database.sqlite && touch tests/database.sqlite`


## Usage

### Starting the server

Laravel comes with a simple PHP server built-in, so we can use that:

`php artisan serve --host=localhost --port=8080`

### Prepping the database initially

`php artisan migrate`

### Creating a user

`curl -X POST -d email=foo@bar.com -d first_name=foo -d last_name=bar -d country=DE -d gender=1 "localhost:8080/users"`

### Updating a user

`curl -X PUT -d first_name=bar -d last_name=baz "localhost:8080/users/1"`

### Viewing a user

`curl -X GET "localhost:8080/users/1"`

### Creating a deposit

`curl -X POST -d user_id=1 -d amount=5 -d transaction_type=1 "localhost:8080/transactions"`

### Creating a withdrawal

`curl -X POST -d user_id=1 -d amount=5 -d transaction_type=2 "localhost:8080/transactions"`

### Viewing a transaction

`curl -X GET "localhost:8080/transactions/1"`

### Viewing a transaction report

`curl "localhost:8080/transactions?aggregate=daily&from_date=2016-11-17T12:12:51%2B0000"`

Parameters:

+	from_date - ISO-8601 string (default 7 days ago)
+	to_date - ISO-8601 string (default now)
+	aggregate - daily (required for report view)

`from_date` and `to_date` parameters are ISO-8601 strings, so make sure the + sign is encoded as %2B in command line

### Viewing a simple list of transactions

`curl "localhost:8080/transactions?order=desc"`

Parameters:

+	from_date - ISO-8601 string (default 7 days ago)
+	to_date - ISO-8601 string (default now)
+	order - asc or desc (default); only valid with no aggregation
+	limit - integer (default 100); number of results to show
+	page - integer (default 1); results page, when used with limit

`from_date` and `to_date` parameters are ISO-8601 strings, so make sure the + sign is encoded as %2B in command line


## Testing

To run all tests:

`phpunit`
