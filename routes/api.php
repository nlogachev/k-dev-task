<?php

/**
 * User-related API routes:
 *
 * POST /users
 * PUT /users/<id>
 * GET /users/<id>
 */
Route::resource('users', 'UserController', ['only' => [
	'store', 'update', 'show'
]]);

/**
 * Transaction-related API routes:
 *
 * POST /transactions
 * GET /transactions
 */
Route::resource('transactions', 'TransactionController', ['only' => [
	'store', 'index', 'show'
]]);