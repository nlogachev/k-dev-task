<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Carbon\Carbon;
use App\Transaction;
use App\User;

class TransactionControllerTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    /**
     * Test to make we don't create any transaction with invalid data
     *
     * @return void
     */
    public function test_transaction_not_created_with_invalid_data()
    {
    	// Send request and make sure JSON structure is fine
        $this->json('POST', '/transactions', ['amount' => -1, 'country' => 'ZZ', 'transaction_type' => Transaction::TYPE_WITHDRAW])
            ->seeJsonStructure([
                'user_id','amount'
            ]);

        // Proper response code
        $this->assertEquals(422, $this->response->status());

        // Make sure data hasn't been updated
        $this->assertEquals(0, DB::table('transactions')->count());
    }

    /**
     * Test to make we can create a valid depost
     *
     * @return void
     */
    public function test_deposit_created_with_valid_data()
    {
    	// Create a test user in the DB
        $user = factory(User::class)->create();

        // For reference
        $balance = $user->balance;
        $balanceBonus = $user->balance_bonus;

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/transactions', ['amount' => 5, 'transaction_type' => Transaction::TYPE_DEPOSIT, 'user_id' => $user->id])
            ->seeJson([
                'success' => true
            ]);

        // Get new models
        $updatedUser = DB::table('users')->where('id',$user->id)->first();
        $transaction = DB::table('transactions')->first();

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        // Make sure data has been properly updated
        $this->assertEquals(1, DB::table('transactions')->count());

        $this->assertEquals($balance + 5, $updatedUser->balance);
        $this->assertEquals($balanceBonus, $updatedUser->balance_bonus);

        $this->assertEquals($user->country, $transaction->country);
        $this->assertEquals(5, $transaction->amount);
        $this->assertEquals($user->id, $transaction->user_id);
        $this->assertEquals(Transaction::TYPE_DEPOSIT, $transaction->transaction_type);

    }

    /**
     * Test to make we give the user a bonus every third deposit
     *
     * @return void
     */
    public function test_third_deposit_yields_bonus()
    {
    	// Create a test user in the DB
        $user = factory(User::class)->create();

        // For reference
        $balance = $user->balance;
        $balanceBonus = $user->balance_bonus;
        $amount = 5;
        $loops = 6;

        for ($i = 0; $i < $loops; $i++) {

	        // Send request and make sure JSON structure is fine
	        $this->json('POST', '/transactions', ['amount' => 5, 'transaction_type' => Transaction::TYPE_DEPOSIT, 'user_id' => $user->id])
	            ->seeJson([
	                'success' => true
	            ]);

        }

        // Get new model
        $updatedUser = DB::table('users')->where('id',$user->id)->first();

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        // Make sure data has been properly updated
        $this->assertEquals($loops, DB::table('transactions')->count());
        $this->assertEquals($balance + $amount * $loops, $updatedUser->balance);
        $this->assertEquals($balanceBonus + $loops / 3 * $user->bonus * $amount, $updatedUser->balance_bonus);

    }

	/**
     * Test to make we can create a valid withdrawal
     *
     * @return void
     */
    public function test_withdrawal_created_with_valid_data()
    {
    	// Create a test user in the DB
        $user = factory(User::class)->create([
        	'balance' => 120,
        	'balance_bonus' => 10
        ]);

        // For reference
        $balance = $user->balance;
        $balanceBonus = $user->balance_bonus;

        // So we know what we're dealing with...
        $this->assertEquals(120, $balance);
        $this->assertEquals(10, $balanceBonus);

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/transactions', ['amount' => 100, 'transaction_type' => Transaction::TYPE_WITHDRAW, 'user_id' => $user->id])
            ->seeJson([
                'success' => true
            ]);

        // Get new models
        $updatedUser = DB::table('users')->where('id',$user->id)->first();
        $transaction = DB::table('transactions')->first();

		// Proper response code
        $this->assertEquals(200, $this->response->status());

        // Make sure data has been properly updated
        $this->assertEquals(1, DB::table('transactions')->count());

        $this->assertEquals(20, $updatedUser->balance);
        $this->assertEquals($balanceBonus, $updatedUser->balance_bonus);

        $this->assertEquals($user->country, $transaction->country);
        $this->assertEquals(100, $transaction->amount);
        $this->assertEquals($user->id, $transaction->user_id);
        $this->assertEquals(Transaction::TYPE_WITHDRAW, $transaction->transaction_type);

    }

    /**
     * Test to make we don't create any transaction for a nonexistant user
     *
     * @return void
     */
    public function test_transaction_fails_with_no_user()
    {
		// Create a test user in the DB
        $user = factory(User::class)->create([
        	'balance' => 120,
        	'balance_bonus' => 10
        ]);

        // For reference
        $balance = $user->balance;
        $balanceBonus = $user->balance_bonus;

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/transactions', ['amount' => 100, 'transaction_type' => Transaction::TYPE_WITHDRAW, 'user_id' => $user->id + 1])
            ->seeJson([
                'error' => 404
            ]);

        // Same user but from db refreshed
        $updatedUser = DB::table('users')->where('id',$user->id)->first();

		// Proper response code
        $this->assertEquals(404, $this->response->status());

        // Make sure data hasn't been updated
        $this->assertEquals(0, DB::table('transactions')->count());

        $this->assertEquals($balance, $updatedUser->balance);
        $this->assertEquals($balanceBonus, $updatedUser->balance_bonus);
    }

    /**
     * Test to make we can't withdraw less than the minimum
     *
     * @return void
     */
    public function test_withdrawal_fails_lower_than_minimum()
    {
    	// Create a test user in the DB
        $user = factory(User::class)->create([
        	'balance' => 120,
        	'balance_bonus' => 10
        ]);

        // For reference
        $balance = $user->balance;
        $balanceBonus = $user->balance_bonus;

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/transactions', ['amount' => Transaction::MIN_WITHDRAWAL - 0.01, 'transaction_type' => Transaction::TYPE_WITHDRAW, 'user_id' => $user->id])
            ->seeJson([
                'error' => 400
            ]);

        // Same user but from db refreshed
        $updatedUser = DB::table('users')->where('id',$user->id)->first();

		// Proper response code
        $this->assertEquals(400, $this->response->status());

        // Make sure data hasn't been updated
        $this->assertEquals(0, DB::table('transactions')->count());

        $this->assertEquals($balance, $updatedUser->balance);
        $this->assertEquals($balanceBonus, $updatedUser->balance_bonus);
    }

    /**
     * Test to make we can't deposit less than the minimum
     *
     * @return void
     */
    public function test_deposit_fails_lower_than_minimum()
    {
    	// Create a test user in the DB
        $user = factory(User::class)->create();

        // For reference
        $balance = $user->balance;
        $balanceBonus = $user->balance_bonus;

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/transactions', ['amount' => Transaction::MIN_DEPOSIT - 0.01, 'transaction_type' => Transaction::TYPE_WITHDRAW, 'user_id' => $user->id])
            ->seeJson([
                'error' => 400
            ]);

        // Same user but from db refreshed
        $updatedUser = DB::table('users')->where('id',$user->id)->first();

		// Proper response code
        $this->assertEquals(400, $this->response->status());

        // Make sure data hasn't been updated
        $this->assertEquals(0, DB::table('transactions')->count());

        $this->assertEquals($balance, $updatedUser->balance);
        $this->assertEquals($balanceBonus, $updatedUser->balance_bonus);
    }

    /**
     * Test to make we can't withdraw more than the balance
     *
     * @return void
     */
    public function test_withdrawal_fails_with_higher_than_balance()
    {
    	// Create a test user in the DB
        $user = factory(User::class)->create([
        	'balance' => 120,
        	'balance_bonus' => 10
        ]);

        // For reference
        $balance = $user->balance;
        $balanceBonus = $user->balance_bonus;

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/transactions', ['amount' => $balance + 0.01, 'transaction_type' => Transaction::TYPE_WITHDRAW, 'user_id' => $user->id])
            ->seeJson([
                'error' => 400
            ]);

        // Same user but from db refreshed
        $updatedUser = DB::table('users')->where('id',$user->id)->first();

		// Proper response code
        $this->assertEquals(400, $this->response->status());

        // Make sure data hasn't been updated
        $this->assertEquals(0, DB::table('transactions')->count());

        $this->assertEquals($balance, $updatedUser->balance);
        $this->assertEquals($balanceBonus, $updatedUser->balance_bonus);
    }

    /**
     * Test to make we can't make have competing transactions simultaneously
     *
     * @return void
     */
    public function test_transaction_fails_when_concurrent()
    {
    	// There are intricate ways of doing this, like with parallel 
    	// requests using icicleio/concurrent, but this is a bit beyond the
    	// scope of this codebase.

    	$this->assertTrue(true);
    }

    /**
     * Test to make sure we can display an existing transaction
     *
     * @return void
     */
    public function test_shows_existing_transaction()
    {
        // Create a test transaction in the DB
        $transaction = factory(Transaction::class)->create();

        // Send request and make sure JSON structure is fine
        $this->json('GET', '/transactions/' . $transaction->id)
            ->seeJson([
                'id' => $transaction->id,
            ]);

        // Proper response code
        $this->assertEquals(200, $this->response->status());
    }

    /**
     * Test to make sure we properly display an error about a nonexistant 
     * transaction
     *
     * @return void
     */
    public function test_nonexisting_transaction()
    {
        // Make sure we know what we're dealing with
        $this->assertEquals(0, DB::table('transactions')->count());

        // Create a test transaction in the DB
        $transaction = factory(Transaction::class)->create();

        // Send request and make sure JSON structure is fine
        $this->json('GET', '/transactions/' . ($transaction->id + 1))
            ->seeJson([
                'error' => 404
            ]);

        // Proper response code
        $this->assertEquals(404, $this->response->status());
    }

    /**
     * Test to make sure we can display a simple list of existing transactions
     *
     * @return void
     */
    public function test_shows_transaction_list()
    {
        // Create a test transaction in the DB
        $transaction1 = factory(Transaction::class)->create();
        $transaction2 = factory(Transaction::class)->create();
        $transaction3 = factory(Transaction::class)->create();

        // Send request and make sure JSON structure is fine
        $this->json('GET', '/transactions', ['page' => 1, 'limit' => 2])
            ->seeJsonStructure([
                '*' => ['id','transaction_type','amount','country','created_at','updated_at','user_id']
            ]);

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        $this->assertEquals(2, count(json_decode((string)$this->response->getContent(),true)));

        // Send request and make sure JSON structure is fine
        $this->json('GET', '/transactions', ['page' => 2, 'limit' => 2])
            ->seeJsonStructure([
                '*' => ['id','transaction_type','amount','country','created_at','updated_at','user_id']
            ]);

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        $this->assertEquals(1, count(json_decode((string)$this->response->getContent(),true)));
    }

    /**
     * Test to make sure we can display a simple list of existing transactions
     *
     * @return void
     */
    public function test_shows_transaction_report()
    {
        // Freeze the time
        Carbon::setTestNow(Carbon::now());

        // Create test deposits in the DB
        $transaction1 = factory(Transaction::class)->create([
            'created_at' => Carbon::now()->subDay(),
            'country' => 'DE',
            'user_id' => 1,
            'transaction_type' => Transaction::TYPE_DEPOSIT,
            'amount' => 100
        ]);
        $transaction2 = factory(Transaction::class)->create([
            'created_at' => Carbon::now()->subDays(2),
            'country' => 'DE',
            'user_id' => 4,
            'transaction_type' => Transaction::TYPE_DEPOSIT,
            'amount' => 150
        ]);
        $transaction3 = factory(Transaction::class)->create([
            'created_at' => Carbon::now()->subDays(2),
            'country' => 'MT',
            'user_id' => 3,
            'transaction_type' => Transaction::TYPE_DEPOSIT,
            'amount' => 175
        ]);
        $transaction3_1 = factory(Transaction::class)->create([
            'created_at' => Carbon::now()->subDays(2),
            'country' => 'MT',
            'user_id' => 3,
            'transaction_type' => Transaction::TYPE_DEPOSIT,
            'amount' => 11
        ]);
        $transaction4 = factory(Transaction::class)->create([
            'created_at' => Carbon::now()->subDay(),
            'country' => 'DE',
            'user_id' => 2,
            'transaction_type' => Transaction::TYPE_DEPOSIT,
            'amount' => 100
        ]);
        $transaction5 = factory(Transaction::class)->create([
            'created_at' => Carbon::now(),
            'country' => 'MT',
            'user_id' => 1,
            'transaction_type' => Transaction::TYPE_DEPOSIT,
            'amount' => 100
        ]);

        // Create test withdrawals in the DB
        $transaction6 = factory(Transaction::class)->create([
            'created_at' => Carbon::now(),
            'country' => 'DE',
            'user_id' => 1,
            'transaction_type' => Transaction::TYPE_WITHDRAW,
            'amount' => 111
        ]);
        $transaction7 = factory(Transaction::class)->create([
            'created_at' => Carbon::now()->subDays(2),
            'country' => 'DE',
            'user_id' => 4,
            'transaction_type' => Transaction::TYPE_WITHDRAW,
            'amount' => 101
        ]);

        // Send request and make sure JSON structure is fine
        $this->json('GET', '/transactions', ['aggregate' => 'daily'])
            ->seeJsonStructure([
                '*' => ['date','country','deposits','withdrawals','withdrawn','deposited','users']
            ]);

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        // Check actual data
        $result = json_decode((string)$this->response->getContent(),true);

        $this->assertEquals(Carbon::now()->format('Y-m-d'), $result[0]['date']); // today DE
        $this->assertEquals(Carbon::now()->format('Y-m-d'), $result[1]['date']); // today MT
        $this->assertEquals(Carbon::now()->subDay()->format('Y-m-d'), $result[2]['date']); // -1 DE
        $this->assertEquals(Carbon::now()->subDays(2)->format('Y-m-d'), $result[3]['date']); // -2 DE
        $this->assertEquals(Carbon::now()->subDays(2)->format('Y-m-d'), $result[4]['date']); // -2 MT
        
        $this->assertFalse(isset($result[5])); // this should be the end of the rows

        $this->assertEquals(1, $result[0]['users']); // today DE
        $this->assertEquals(1, $result[1]['users']); // today MT
        $this->assertEquals(2, $result[2]['users']); // -1 DE
        $this->assertEquals(1, $result[3]['users']); // -2 DE
        $this->assertEquals(1, $result[4]['users']); // -2 MT

        $this->assertEquals(0, $result[0]['deposited']); // today DE
        $this->assertEquals(100, $result[1]['deposited']); // today MT
        $this->assertEquals(200, $result[2]['deposited']); // -1 DE
        $this->assertEquals(150, $result[3]['deposited']); // -2 DE
        $this->assertEquals(186, $result[4]['deposited']); // -2 MT

        $this->assertEquals(0, $result[0]['deposits']); // today DE
        $this->assertEquals(1, $result[1]['deposits']); // today MT
        $this->assertEquals(2, $result[2]['deposits']); // -1 DE
        $this->assertEquals(1, $result[3]['deposits']); // -2 DE
        $this->assertEquals(2, $result[4]['deposits']); // -2 MT

        $this->assertEquals(-111, $result[0]['withdrawn']); // today DE
        $this->assertEquals(0, $result[1]['withdrawn']); // today MT
        $this->assertEquals(0, $result[2]['withdrawn']); // -1 DE
        $this->assertEquals(-101, $result[3]['withdrawn']); // -2 DE
        $this->assertEquals(0, $result[4]['withdrawn']); // -2 MT

        $this->assertEquals(1, $result[0]['withdrawals']); // today DE
        $this->assertEquals(0, $result[1]['withdrawals']); // today MT
        $this->assertEquals(0, $result[2]['withdrawals']); // -1 DE
        $this->assertEquals(1, $result[3]['withdrawals']); // -2 DE
        $this->assertEquals(0, $result[4]['withdrawals']); // -2 MT
    }
}
