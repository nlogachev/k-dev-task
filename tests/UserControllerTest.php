<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;

class UserControllerTest extends TestCase
{
    use DatabaseTransactions;
    use DatabaseMigrations;

    /**
     * Test to make we don't create any user with invalid input
     *
     * @return void
     */
    public function test_user_not_created_with_invalid_data()
    {
        // Send request and make sure JSON structure is fine
        $this->json('POST', '/users', ['last_name' => 'a', 'email' => 'foo'])
            ->seeJsonStructure([
                'email','gender','first_name','last_name','country'
            ]);

        // Proper response code
        $this->assertEquals(422, $this->response->status());

        // Make sure no data has been set
        $this->assertEquals(0, DB::table('users')->count());
    }

    /**
     * Test to make sure we properly create user with valid input
     *
     * @return void
     */
    public function test_user_created_with_valid_data()
    {
        // For reference
        $email = 'foo@bar.com';
        $firstName = 'foo';
        $lastName = 'bar';
        $country = 'DE';
        $gender = User::GENDER_MALE;

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/users', [
            'first_name' => $firstName,
            'last_name' => $lastName,
            'email' => $email,
            'country' => $country,
            'gender' => $gender,
        ])->seeJson([
            'success' => true
        ]);

        // Updated user model from the db
        $user = DB::table('users')->where('email', $email)->first();

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        // Make sure data has been set
        $this->assertNotNull($user);

        // Technically this should be tested separately on the user model,
        // but it helps for peace of mind to make sure the FUNCTIONALITY
        // works here as well.
        $this->assertEquals($firstName, $user->first_name);
        $this->assertEquals($lastName, $user->last_name);
        $this->assertEquals($gender, $user->gender);
        $this->assertEquals($email, $user->email);
        $this->assertEquals($country, $user->country);
        $this->assertEquals(0, $user->balance);
        $this->assertEquals(0, $user->balance_bonus);
    }

    /**
     * Test to make sure email is unique per user
     *
     * @return void
     */
    public function test_user_not_created_with_duplicate_email()
    {
        // For reference
        $email = 'foo@bar.com';

        // Send request and make sure JSON structure is fine
        $this->json('POST', '/users', [
            'first_name' => 'foo',
            'last_name' => 'bar',
            'email' => $email,
            'country' => 'DE',
            'gender' => User::GENDER_MALE,
        ])->seeJson([
            'success' => true
        ]);

        // Just so we know what we're dealing with
        $this->assertNotNull(DB::table('users')->where('email', $email)->first());
        $this->assertEquals(200, $this->response->status());

        // Send request and make sure JSON structure is fine
        $response = $this->json('POST', '/users', [
            'first_name' => 'foo1',
            'last_name' => 'bar1',
            'email' => $email,
            'country' => 'MT',
            'gender' => User::GENDER_FEMALE,
        ])->seeJson([
            'error' => 409
        ]);

        // Proper response code
        $this->assertEquals(409, $this->response->status());

        // Make sure there's still only 1 user
        $this->assertEquals(1, DB::table('users')->where('email', $email)->count());

    }

    /**
     * Test to make sure an existing user can't be updated with invalid input
     *
     * @return void
     */
    public function test_user_not_updated_with_invalid_data()
    {
        // Create a test user in the DB
        $user = factory(User::class)->create();

        // Make sure we know what we're dealing with
        $this->assertEquals('DE', $user->country);

        // Keep references
        $email = $user->email;
        $gender = $user->gender;

        // Send request and make sure JSON structure is fine
        $this->json('PUT', '/users/' . $user->id, ['email' => 'foo', 'gender' => 999, 'country' => 'MT'])
            ->seeJsonStructure([
                'email','gender'
            ]);

        // Get the user in the db
        $user = DB::table('users')->where('id',$user->id)->first();

        // Proper response code
        $this->assertEquals(422, $this->response->status());

        // Make sure the data hasn't changed in the db
        $this->assertEquals('DE', $user->country);
        $this->assertEquals($gender, $user->gender);
        $this->assertEquals($email, $user->email);
    }

    /**
     * Test to make sure unexpected data doesn't enter the database
     *
     * @return void
     */
    public function test_user_not_updated_with_unknown_data()
    {
        // Create a test user in the DB
        $user = factory(User::class)->create();

        // Make sure we know what we're dealing with
        $this->assertNull($user->foo);

        // Send request and make sure JSON structure is fine
        $this->json('PUT', '/users/' . $user->id, ['foo' => 'foo'])
            ->seeJson([
                'success' => true
            ]);

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        // Get the user in the db
        $user = DB::table('users')->where('id',$user->id)->first();

        // Make sure unknown data hasn't been inserted
        $this->assertTrue(!isset($user->foo));
    }

    /**
     * Test to make sure an existing user can be updated with valid data
     *
     * @return void
     */
    public function test_user_updated_with_valid_data()
    {
        // Create a test user in the DB
        $user = factory(User::class)->create();

        // Keep references
        $firstName = $user->first_name;
        $newName = 'winner winner chicken dinner';

        // Make sure we know what we're dealing with
        $this->assertNotEquals($user->first_name, $newName);

        // Send request and make sure JSON structure is fine
        $this->json('PUT', '/users/' . $user->id, ['first_name' => $newName])
            ->seeJson([
                'success' => true
            ]);

        // Proper response code
        $this->assertEquals(200, $this->response->status());

        // Get the user in the db
        $user = DB::table('users')->where('id',$user->id)->first();

        // Make sure unknown data hasn't been inserted
        $this->assertEquals($newName, $user->first_name);
    }

    /**
     * Test to make sure user's balance cannot be updated directly
     *
     * @return void
     */
    public function test_user_balances_cannot_be_updated_through_api()
    {
        // Create a test user in the DB
        $user = factory(User::class)->create();

        // Keep references
        $balance = $user->balance;
        $bonus = $user->balance_bonus;
        $newBalance = 777;
        $newBonus = 0.777;

        // Make sure we know what we're dealing with
        $this->assertNotEquals($newBalance, $user->balance);
        $this->assertNotEquals($newBonus, $user->balance_bonus);

        // Send request and make sure JSON structure is fine
        $this->json('PUT', '/users/' . $user->id, ['balance' => $newBalance, 'balance_bonus' => $newBonus])
            ->seeJson([
                'error' => 400
            ]);

        // Proper response code
        $this->assertEquals(400, $this->response->status());

        // Get the user in the db
        $user = DB::table('users')->where('id',$user->id)->first();

        // Make sure data hasn't been updated
        $this->assertEquals($balance, $user->balance);
        $this->assertEquals($bonus, $user->balance_bonus);
    }

    /**
     * Test to make sure we can display an existing user
     *
     * @return void
     */
    public function test_shows_existing_user()
    {
        // Create a test user in the DB
        $user = factory(User::class)->create();

        // Send request and make sure JSON structure is fine
        $this->json('GET', '/users/' . $user->id)
            ->seeJson([
                'first_name' => $user->first_name
            ]);

        // Proper response code
        $this->assertEquals(200, $this->response->status());
    }

    /**
     * Test to make sure we properly display an error about a nonexistant user
     *
     * @return void
     */
    public function test_nonexisting_user()
    {
        // Make sure we know what we're dealing with
        $this->assertEquals(0, DB::table('users')->count());

        // Create a test user in the DB
        $user = factory(User::class)->create();

        // Send request and make sure JSON structure is fine
        $this->json('GET', '/users/' . ($user->id + 1))
            ->seeJson([
                'error' => 404
            ]);

        // Proper response code
        $this->assertEquals(404, $this->response->status());
    }
}
