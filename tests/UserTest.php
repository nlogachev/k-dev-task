<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\User;

class UserTest extends TestCase
{
    /**
     * Test to make sure our random user bonus stays within its expected limits
     *
     * @return void
     */
    public function test_random_user_bonus_is_within_limits()
    {
        $found = false;

        // 250k runs
        for ($i = 0; $i < 250000; $i++) {
            $bonus = User::randomBonusAmount();

            if ($bonus < User::BONUS_MIN || $bonus > User::BONUS_MAX) {
            	$found = true;
            	break;
            }
        }

        $this->assertFalse($found);
    }

    /**
     * Test to make sure our random user bonus is actually random
     *
     * @return void
     */
    public function test_random_user_bonus_is_actually_random()
    {
        $found = [];

        // 10k runs
        for ($i = 0; $i < 10000; $i++) {
            $bonus = User::randomBonusAmount();

            $found[(string)$bonus] = true;
        }

        // Just a best guess for initial peace of mind. Due to the nature of 
        // the RNG, we might not actually get all values from 5-20.
        $this->assertTrue(count($found) >= 10);
    }
}
